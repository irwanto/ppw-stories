from django.urls import path
from . import views

urlpatterns = [
    path('', views.home, name='index'),
    path('home', views.home, name='home'),
    path('about', views.about, name='about'),
    path('register', views.form, name='form'),
    path('register_failed', views.register_failed, name='register_failed')
]
