from django.db import models
from django.utils import timezone

class JadwalPribadi(models.Model):
    nama_kegiatan = models.CharField(max_length = 50)
    tanggal = models.DateField(default=timezone.now)
    waktu = models.TimeField(default=timezone.now)
    tempat = models.CharField(max_length = 50)
    kategori = models.CharField(max_length = 50)
