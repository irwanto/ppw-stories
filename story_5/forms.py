from django import forms

class FormJadwal(forms.Form):
    attrs = {
        'class': 'form-control',
        'style': 'width: 40%',
        'align-items': 'center'
    }

    date_attrs = {
        'class':'form-control',
        'type':'date',
        'style': 'width: 40%'
    }

    time_attrs = {
        'class':'form-control',
        'type': 'time',
        'style': 'width: 40%'
    }

    nama_kegiatan = forms.CharField(label="Nama kegiatan", required=True, max_length=50, widget=forms.TextInput(attrs=attrs))
    tanggal = forms.DateField(label="Tanggal kegiatan", required=True, widget=forms.DateInput(attrs=date_attrs))
    waktu = forms.TimeField(label="Waktu kegiatan", required=True, widget=forms.TimeInput(attrs=time_attrs))
    tempat = forms.CharField(label="Tempat", required=True, max_length=50, widget=forms.TextInput(attrs=attrs))
    kategori = forms.CharField(label="Kategori", required=True, max_length=50, widget=forms.TextInput(attrs=attrs))
