from django.urls import path
from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('tambah_kegiatan', views.tambah_kegiatan, name='tambah_kegiatan'),
    path('tampilkan_jadwal', views.tampilkan_jadwal, name='tampilkan_jadwal'),
    path('delete_all', views.delete_all, name='delete_all')
]
