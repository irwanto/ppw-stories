from django.shortcuts import render

def home(request):
    return render(request, 'story_4/story-3-home.html', {})

def about(request):
    return render(request, 'story_4/story-3-about.html', {})

def form(request):
    return render(request, 'story_4/challenge-3.html', {})

def register_failed(request):
    return render(request, 'story_4/forms-saved.html', {})
