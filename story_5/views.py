from django.shortcuts import render
from django.http import HttpResponseRedirect
from .models import JadwalPribadi
from .forms import FormJadwal

def index(request):
    form = FormJadwal()
    return render(request, 'index.html', {'form': form})

def tambah_kegiatan(request):
    form = FormJadwal(request.POST or None)
    if (request.method == 'POST' and form.is_valid()):
        nama_kegiatan = request.POST['nama_kegiatan']
        tanggal = request.POST['tanggal']
        waktu = request.POST['waktu']
        tempat = request.POST['tempat']
        kategori = request.POST['kategori']

        jadwal = JadwalPribadi(nama_kegiatan=nama_kegiatan, tanggal=tanggal, waktu=waktu, tempat=tempat, kategori=kategori)
        jadwal.save()
        return render(request, 'form-saved.html', {})
    else:
        return HttpResponseRedirect('/story_5/')

def tampilkan_jadwal(request):
    semua_jadwal = JadwalPribadi.objects.all()
    return render(request, 'result.html', {'semua_jadwal': semua_jadwal})

def delete_all(request):
    JadwalPribadi.objects.all().delete()
    return render(request, 'result.html', {})
